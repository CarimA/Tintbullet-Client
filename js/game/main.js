// todo: make a chime on posting/logging in/out
// todo: preload images.
// todo: add mute/ip ban commands
// todo: http://i43.tinypic.com/14b6m80.png - display players online + capacity + server status
// todo: add a wait overlay to attempting to log in.
/*$("#overlay").fadeTo(fadeWait, 0.6);
$("#overlay").fadeTo(fadeWait, 0, function() {
	// clear display
	document.getElementById('overlay').style.display = "none";
});*/
// todo: scroll to bottom on chat
// todo: fix credits button
// todo: check if user is already logged on.
// todo: lobby

var windowX = 1138;
var windowY = 640;

var fadeWait = 1000;

var versionMajor = 1;
var versionMinor = 0;

var renderer = new PIXI.autoDetectRenderer(windowX, windowY);

var stages = {};
var currentStage;

var gameInitialised;

function main() {
	gameInitialised = false;

	document.getElementById('canvas').appendChild(this.renderer.view);
		
	PIXI.scaleModes.DEFAULT = PIXI.scaleModes.NEAREST;
	
	// Create the window events we need.
	window.addEventListener("resize", resize);
	window.addEventListener('keyup', function(event) { Key.onKeyup(event); }, false);
	window.addEventListener('keydown', function(event) { Key.onKeydown(event); }, false);
	resize();

	$('#chat_box_content').perfectScrollbar();

	setState('connecting');
	setTimeout(function() {
		AlchemyChatServer.Start();
	}, fadeWait * 2.5);
	//setState('mainmenu');
	
	requestAnimationFrame(update);
}

function setState(id)
{
	// set display of overlay.
	$("#overlay").fadeTo(fadeWait, 1, function() {
		// destroy here.
		if (currentStage)
			currentStage.destroy();
			
		currentStage = stages[id];
		
		// set state here.
		currentStage.init();
	}).fadeTo(fadeWait, 0, function() {
		// clear display
		document.getElementById('overlay').style.display = "none";
	});
}

function resize(){
	var scale = 1;
	//if (window.innerWidth  > window.innerHeight) {
	if (window.innerHeight / window.innerWidth < 0.562390158172232) {
		// it's wider, so take the full height.
		renderer.view.style.height = (window.innerHeight * scale) + "px";
		renderer.view.style.width = (((window.innerHeight * scale) / windowY) * windowX) + "px";
		document.getElementById('canvas').style.marginTop = (-(window.innerHeight * scale) / 2) + "px";
		document.getElementById('canvas').style.marginLeft = (-(((window.innerHeight * scale) / windowY) * windowX) / 2) + "px";
		
		document.getElementById('chat_wrapper').style.height = (window.innerHeight * scale) + "px";
		//document.getElementById('chat_wrapper').style.width = (((window.innerHeight * scale) / windowY) * windowX) + "px";
		//document.getElementById('chat_wrapper').style.marginTop = (-(window.innerHeight * scale) / 2) + "px";
		//document.getElementById('chat_wrapper').style.marginLeft = (-(((window.innerHeight * scale) / windowY) * windowX) / 2) + "px";
	}
	else
	{
		// it's taller, take up the full width.
		renderer.view.style.width = (window.innerWidth * scale) + "px";
		renderer.view.style.height = (((window.innerWidth * scale) / windowX) * windowY) + "px";
		document.getElementById('canvas').style.marginLeft = (-(window.innerWidth * scale) / 2) + "px";
		document.getElementById('canvas').style.marginTop = (-(((window.innerWidth * scale) / windowX) * windowY) / 2) + "px";
		
		//document.getElementById('chat_wrapper').style.width = (window.innerWidth * scale) + "px";
		document.getElementById('chat_wrapper').style.height = (((window.innerWidth * scale) / windowX) * windowY) + "px";
		//document.getElementById('chat_wrapper').style.marginLeft = (-(window.innerWidth * scale) / 2) + "px";
		//document.getElementById('chat_wrapper').style.marginTop = (-(((window.innerWidth * scale) / windowX) * windowY) / 2) + "px";
	}
	
}

function update() {
	//robot.rotation += 0.01;
	if (currentStage)
	{
		currentStage.update();
		renderer.render(currentStage.stage);
	}
	requestAnimationFrame(update);
}

function setTextbox(id, x, y, visible) {
	var el = document.getElementById(id);
	el.style.left = x + "px";
	el.style.top = y + "px";	
	el.style.display = visible;
}

function getTextbox(id) {
	return document.getElementById(id).value;	
}

function setTextboxText(id, value) {
	 document.getElementById(id).value = value;	
}

var Key = {
  _pressed: {},
  ENTER: 13,
  LEFT: 37,
  UP: 38,
  RIGHT: 39,
  DOWN: 40,
  
  isDown: function(keyCode) {
    return this._pressed[keyCode];
  },
  
  onKeydown: function(event) {
    this._pressed[event.keyCode] = true;
  },
  
  onKeyup: function(event) {
    delete this._pressed[event.keyCode];
  }
};

