stages['connecting'] = { 
	stage: new PIXI.Stage(0x000000, true),
	
	init:function() {
		this.spinner = new PIXI.Sprite(PIXI.Texture.fromImage("assets/graphics/spinner.png"));
		this.spinner.x = windowX / 2;
		this.spinner.y = windowY / 2;
		this.spinner.anchor.x = 0.5;
		this.spinner.anchor.y = 0.5;
		this.stage.addChild(this.spinner);
		
		this.conn = new PIXI.Text("Connecting to the Tintbullet server...", {font:"26px Euphemia", fill:"white", align:"center"});
		this.conn.anchor.x = 0.5;
		this.conn.anchor.y = 0.5;
		this.conn.x = windowX / 2;
		this.conn.y = windowY / 2;
		this.stage.addChild(this.conn);

		//this.shouldSpin = true;
		//setTimeout(function() {
		//	if (currentStage == stages['connecting'])
		//	{
		//		// we're still here. tell 'em we can't connect and stop trying.
		//		this.conn = new PIXI.Text("Could not connect to the Tintbullet server...\r\n" +
		//		"The server may have gone down, or your internet connection may be unstable.\r\n" +
		//		"Retry by refreshing shortly.")
		//		AlchemyChatServer.Stop();
		//		this.shouldSpin = false;
		//	}
		//}, 10000);
	},
	
	update:function() {
		//if (this.shouldSpin)
			this.spinner.rotation += 0.1;
	},
	
	destroy:function() {
		this.conn = null;
		this.spinner = null;
	}
};

stages['splash1'] = { 
	stage: new PIXI.Stage(0x000000, true),
	
	init:function() {
		this.background = new PIXI.Sprite(PIXI.Texture.fromImage("assets/graphics/splash1.png"));
		this.background.x = windowX / 2 - 154;
		this.background.y = windowY / 2 - 153;
		this.stage.addChild(this.background);
		
		setTimeout(function() {
			setState('splash2');
		}, 2000);
	},
	
	update:function() {
	},
	
	destroy:function() {
		this.background = null;
	}
};

stages['splash2'] = { 
	stage: new PIXI.Stage(0x000000, true),
	
	init:function() {
		this.background = new PIXI.Sprite(PIXI.Texture.fromImage("assets/graphics/splash2.png"));
		this.background.x = windowX / 2 - 90;
		this.background.y = windowY / 2 - 6;
		this.stage.addChild(this.background);
		
		setTimeout(function() {
			setState('splash3');
		}, 4000);
	},
	
	update:function() {
	},
	
	destroy:function() {
		this.background = null;
	}
};

stages['splash3'] = { 
	stage: new PIXI.Stage(0x000000, true),
	
	init:function() {		
		this.background = new PIXI.Sprite(PIXI.Texture.fromImage("assets/graphics/logo.png"));
		this.background.x = windowX / 2 - 373;
		this.background.y = windowY / 2 - 261;
		
		this.stage.addChild(this.background);

		var sound = new Howl({
		  urls: ['assets/audio/music/mainmenu.mp3'],
		  autoplay: true,
		  loop: true,
		  volume: 1,
		}).play();

		setTimeout(function() {
			setState('mainmenu');
		}, 4000);
	},
	
	update:function() {
	},
	
	destroy:function() {
		this.background = null;
	}
};