stages['mainmenu'] = { 
	stage: new PIXI.Stage(0x000000, true),
	
	init:function() {
		this.background = new PIXI.Sprite(PIXI.Texture.fromImage("assets/graphics/interface/mainmenu/background.png"));
		this.background.x = 0;
		this.background.y = 0;
		this.stage.addChild(this.background);
		
		this.footer = new PIXI.Sprite(PIXI.Texture.fromImage("assets/graphics/interface/mainmenu/footer.png"));
		this.footer.x = 0;
		this.footer.y = windowY - 40;
		this.stage.addChild(this.footer);
		
		this.version = new PIXI.Text("Version " + versionMajor + "." + versionMinor, {font:"14px Euphemia", fill:"white", align:"left"});
		this.version.anchor.x = 0;
		this.version.anchor.y = 0.5;
		this.version.x = 10;
		this.version.y = windowY - 18;
		this.stage.addChild(this.version);
		
		this.copyright = new PIXI.Text("Copyright © 2014 Glaciate & Tintbullet. All rights reserved.", {font:"14px Euphemia", fill:"white", align:"right"});
		this.copyright.anchor.x = 1;
		this.copyright.anchor.y = 0.5;
		this.copyright.x = windowX - 10;
		this.copyright.y = windowY - 18;
		this.stage.addChild(this.copyright);

		this.box = new PIXI.Sprite(PIXI.Texture.fromImage("assets/graphics/interface/mainmenu/box.png"));
		this.box.x = 768;
		this.box.y = 309;
		this.stage.addChild(this.box);
		
		this.login_button = addButton(789, 330, "assets/graphics/interface/mainmenu/button-login-normal.png", "assets/graphics/interface/mainmenu/button-login-hover.png",
"assets/graphics/interface/mainmenu/button-login-click.png", function(){
		vex.dialog.open({
               message: 'Enter your username and password:',
               input: "<input name='username' type='text' placeholder='Username' required /><br><input name='password' type='password' placeholder='Password' required />",
               buttons: [
              	$.extend({}, vex.dialog.buttons.YES, { text: 'Login' }),
                   $.extend({}, vex.dialog.buttons.NO, { text: 'Back' })
               ],
               callback: function (data) {
				if (data)
					sendLogin(data.username, data.password);
               }
		})});
		this.stage.addChild(this.login_button);
		
		this.register_button = addButton(789, 400, "assets/graphics/interface/mainmenu/button-register-normal.png", "assets/graphics/interface/mainmenu/button-register-hover.png",
"assets/graphics/interface/mainmenu/button-register-click.png", function(){
			window.open("http://glaciate.net/forum/member.php?action=register", '_blank');
		});
		this.stage.addChild(this.register_button);
				
		this.delimiter = new PIXI.Sprite(PIXI.Texture.fromImage("assets/graphics/interface/mainmenu/delimiter.png"));
		this.delimiter.x = 818;
		this.delimiter.y = 474;
		this.stage.addChild(this.delimiter);
		
		this.credits_button = addButton(789, 490, "assets/graphics/interface/mainmenu/button-credits-normal.png", "assets/graphics/interface/mainmenu/button-credits-hover.png",
"assets/graphics/interface/mainmenu/button-credits-click.png", function(){
			window.open("http://glaciate.net/forum/member.php?action=register", '_blank');
		});
		this.stage.addChild(this.credits_button);
	},
	
	update:function() {
	},
	
	destroy:function() {
		this.background = null;
		this.footer = null;
		this.version = null;
		this.copyright = null;
		this.login_button = null;
		this.register_button = null;
		this.credits_button = null;
	}
};

function addButton(x, y, normal, hover, down, action) {
	var button_normal = PIXI.Texture.fromImage(normal);
	var button_hover = PIXI.Texture.fromImage(hover);
	var button_click = PIXI.Texture.fromImage(down);
		
	button = new PIXI.Sprite(button_normal);
	button.x = x;
	button.y = y;
	button.interactive = true;
	button.mousedown = button.touchstart = function(data) {
		this.isdown = true;
		this.setTexture(button_click);
	};
	button.mouseup = button.touchend = function(data) {
		this.isdown = false;
		if (this.isover)
			this.setTexture(button_hover);
		else
			this.setTexture(button_normal);	
	};
	button.mouseover = function(data) {
		this.isover = true;
		if (this.isdown) return;
		this.setTexture(button_hover);	
	};
	button.mouseout = function(data) {
		this.isover = false;
		this.isdown = false;
		this.setTexture(button_normal);	
	};
	button.click = button.tap = action;
	return button;
}