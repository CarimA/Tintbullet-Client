stages['lobby'] = { 
	stage: new PIXI.Stage(0x000000, true),
	
	init:function() {
		document.getElementById('chat_wrapper').style.display = 'block';
		
		this.spinner = new PIXI.Sprite(PIXI.Texture.fromImage("assets/graphics/spinner.png"));
		this.spinner.x = windowX / 2;
		this.spinner.y = windowY / 2;
		this.spinner.anchor.x = 0.5;
		this.spinner.anchor.y = 0.5;
		this.stage.addChild(this.spinner);
	},
	
	update:function() {
		if (Key.isDown(Key.ENTER))
		{
			// We hit enter!
			if (document.getElementById('chat_input_text').value)
			{
				sendData("Chat", { Message: document.getElementById('chat_input_text').value });
				document.getElementById('chat_input_text').value = null;
			}
		}
	},
	
	destroy:function() {
		
	}
};

function scrollToBottom()
{
	//var objDiv = document.getElementById("chat_box_content");
	//objDiv.scrollTop = objDiv.scrollHeight;

	$("#chat_box_content").animate({ scrollTop: $("#chat_box").height() }, 80);
	$('#chat_box_content').perfectScrollbar('update');
}