﻿// Set up the Alchemy client object

AlchemyChatServer = new Alchemy({
    Server: "localhost", // "85.222.228.199",
    Port: 5204,
    Action: "chat",
    DebugMode: true
});

AlchemyChatServer.Connected = function () {
	setState('splash1');
    gameInitialised = true;
};

AlchemyChatServer.Disconnected = function () {
    if (gameInitialised)
        vex.dialog.alert("<strong>Uh oh...</strong><br>You have disconnected from the server.<br>The server may have gone down, or your internet connection may be unstable.<br><br>Try reconnecting by refreshing shortly.");
    else
        vex.dialog.alert("<strong>Uh oh...</strong><br>Could not connect to the server.<br>The server may be down, or your internet connection may be unstable.<br><br>Retry shortly by refreshing.");
};

AlchemyChatServer.MessageReceived = function (event) {
    var rcv = JSON.parse(event.data);
	switch (rcv.Type)
	{
		case "Notification":
			vex.dialog.alert(rcv.Data.Message);
			break;
			
		case "ValidateLogin":
			setState('lobby');
			break;
			
		case "Chat":
			document.getElementById('chat_box').innerHTML += "<div class='chat_line'>" + rcv.Data.Message + "</div>";
            scrollToBottom();
			break;

        case "OnlineList":
            document.getElementById('chat_online_users').innerHTML = rcv.Data.Data;
            break;
	}
};

function sendData(type, data) {
    var d = { Type: type, Data: data };
    AlchemyChatServer.Send(d);
}

function sendLogin(username, password) {
    if (!username) {
        vex.dialog.alert("Please enter a username.");
        return;
    }
	
    if (!password) {
        vex.dialog.alert("Please enter a password.");
        return;
    }

    sendData("Login", { Username: username, Password: CryptoJS.MD5(password).toString()});
}

